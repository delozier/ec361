#define DL 0						// TODO: Fix this
#define DIVADDVAL 0			// TODO: Fix this
#define MULVAL 0				// TODO: Fix this

unsigned int *PCONP_ptr = (unsigned int*)0x400FC0C4;
unsigned int *PCLKSEL0_ptr = (unsigned int*)0x400FC1A8;
unsigned int *PINSEL0_ptr = (unsigned int*)0x4002C000;
unsigned int *U0LCR_ptr = (unsigned int*)0x4000C00C;
unsigned int *U0DLL_ptr = (unsigned int*)0x4000C000;
unsigned int *U0DLM_ptr = (unsigned int*)0x4000C004;
unsigned int *U0FDR_ptr = (unsigned int*)0x4000C028;
unsigned int *U0FCR_ptr = (unsigned int*)0x4000C008;
unsigned int *U0LSR_ptr = (unsigned int*)0x4000C014;
unsigned int *U0THR_ptr = (unsigned int*)0x4000C000;

void UART0_Init(){
	// Enable ADC power: PCONP(3) = 1
	// Set PCLK for UART0 to CCLK = 100 MHz: PCLKSEL0(7:6) = 01
	// Set U0LCR to use 8-bit length, 1 stop bit, no parity, enable divisor latch
	// Set U0DLL
	// Set U0DLM
	// Set DIVADDVAL and MULVAL via U0FDR
	// Enable FIFOS: U0FCR(0) = 1
	// Configure P0.2 to function as UART0 TXD0: PINSEL0(5:4) = 01
	// Configure P0.3 to function as UART0 RXD0: PINSEL0(7:6) = 01
	// Disable divisor latch access: U0LCR(7) = 0
}

int	main(){
	UART0_Init();
	while(1){
		// Check if THRE bit == 1 (ready to send) (bit 5 of U0LSR)
		// Send 'T' using UOTHR
	}
}