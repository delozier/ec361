#include <math.h>
#define WAVE_SAMPLE_NUM 64
#define PI (float)(3.141592654)

int sinusoid[WAVE_SAMPLE_NUM];

struct {
	unsigned int source;
	unsigned int destination;
	unsigned int next;
	unsigned int control;
} DMA_LL;

void GPDMA_Init(){
	unsigned int *PCONP_ptr = (unsigned int*)0x400FC0C4;
	unsigned int *DMACConfig = (unsigned int*)0x50004030;
	unsigned int *DMACC0SRCAddr = (unsigned int*)0x50004100;
	unsigned int *DMACC0DestAddr = (unsigned int*)0x50004104;
	unsigned int *DMACC0LLI = (unsigned int*)0x50004108;
	unsigned int *DMACC0Control = (unsigned int*)0x5000410C;
	unsigned int *DMACC0Config = (unsigned int*)0x50004110;
	
	// Power up DMA: PCONP(29) = 1
	// Enable the GPDMA and use little endian mode DMACConfig = 1
	
	// Set up linked list in case we want to use it later (other examples)
	
	// Set address of data array
	// Set address of DACR register as destination
	// Set address for next node in linked list
	// Optional: Set DMACC0LLI = 0 if it only runs once
	// Set DMACC0Control
	// Set DMACC0Config
}

void DAC_DMA_Init(){
	unsigned int *PINSEL1_ptr = (unsigned int*)0x4002C004;
	unsigned int *PINMODE1_ptr = (unsigned int*)0x4008C044;
	unsigned int *DACCNTVAL_ptr = (unsigned int*)0x4008C008;
	unsigned int *DACCTRL_ptr = (unsigned int*)0x4008C004;
	unsigned int *PCLKSEL0_ptr = (unsigned int*)0x400FC1A8;
	
	// Set pin P0.26 to be AOUT
	// Disable pull up and pull down resistors: PINMODE1(21:20) = 10
	// Set PCLK_DAC = 100/4 MHz: PCLKSEL0(23:22) = 00
	// Set DACCTVAL = 250 = 10 us interval
	// Enable DMA, timer, double buffering: DACCTRL(3:1) = 111
}

int main(){
	int i;
	for(i = 0; i < WAVE_SAMPLE_NUM; i++){
		sinusoid[i] = 512*sin(2 * PI * i / WAVE_SAMPLE_NUM);
		sinusoid[i] = ((sinusoid[i] + 512) << 6); // DACR(15:6) = value
	}
	
	DAC_DMA_Init();
	GPDMA_Init();
	
	while(1) {}
}