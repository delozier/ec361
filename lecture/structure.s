; Lesson 10 in-class exercise
		AREA mycode, CODE, READONLY
		EXPORT __main
		ALIGN
		ENTRY
__main 	PROC
		
		
		; initialization
		MOV R6, #1
		MOV R5, #0
		LDR R4, =0x2009C034
main_loop
			
wait_release

		B 	main_loop
		ENDP
		
port_config PROC
		LDR R1, =0x2009C020
		
		BX LR
		ENDP
		
		; Assumes that R0 holds the input
LEDs	PROC
		MOV R3, #0
		AND R1, R0, #0x80
		LSL R1, R1, #21
		ORR R3, R3, R1
		AND R1, R0, #0x40
		LSL R1, R1, #23
		ORR R3, R3, R1
		AND R1, R0, #0x20
		LSL R1, R1, #26
		ORR R3, R3, R1
		LDR R2, =0x2009C034
		STR R3, [R2]
		
		MOV R3, #0
		AND R1, R0, #0x10
		LSR R1, R1, #2
		ORR R3, R3, R1
		AND R1, R0, #0x08
		ORR R3, R3, R1
		AND R1, R0, #0x04
		LSL R1, R1, #2
		ORR R3, R3, R1
		AND R1, R0, #0x02
		LSL R1, R1, #4
		ORR R3, R3, R1
		AND R1, R0, #0x01
		LSL R1, R1, #6
		ORR R3, R3, R1
		STR R3, [R2, #0x20]
		BX LR
		ENDP

		END