extern void port_config();
extern void LEDs(int N);

void ADC_Init(){
	volatile unsigned int* PCONP_ptr = (unsigned int*)0x400FC0C4;
	volatile unsigned int* PCLKSEL0_ptr = (unsigned int*)0x400FC1A8;
	volatile unsigned int* PINSEL3_ptr = (unsigned int*)0x4002C00C;
	volatile unsigned int* AD0CR_ptr = (unsigned int*)0x40034000;
	
	// Enable ADC power
	
	// Set PCLK for ADC to 1/8 CCLK = 12.5 MHz
	
	// Enable P1.31 pin as AD0.5 channel
	
	// Select channel AD0.5 -> bit 5 = 1
	// A/D is operational -> bit 21 = 1
	// Start by software code -> bits 26:24 = 000
	
}

int main(){
	ADC_Init();
	
	volatile unsigned int* AD0CR_ptr = (unsigned int*)0x40034000;
	volatile unsigned int* AD0DR5_ptr = (unsigned int*)0x40034024;
	
	while(1){
		// Start conversion process
		
		// Wait until result is ready
		
		// Output 8 MSBs of ADC result to LEDs
	}
}
