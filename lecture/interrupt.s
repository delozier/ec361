; Lesson 11 in-class exercise
		AREA mydata, DATA, READWRITE
count	DCD 0

		AREA mycode, CODE, READONLY
		EXPORT __main
		ALIGN
		ENTRY
__main 	PROC
		LDR R0, =count 	; R0 = address of count in data memory
		MOV R1, #0 		; R1 = 0
		STR R1, [R0]	; Initialize count to 0 (by storing)
		BL Init_LEDs	; Initialize LED display
		BL EINT0_Init	; Call EINT0_Init
loop	B loop
		ENDP

EINT0_Init PROC
; Step 1: Setup pin to be EINT0
; PINSEL4(21:20) = 01, other bits unchanged
		MOV R0, #1				; R0 = bit mask
		; R2 = address of PINSEL4 register
		; R3 = current value of PINSEL4
		; Set bit 20, leave other bits unchanged
		; Clear bit 21, leave other bits unchanged
		; Write back to PINSEL4 register
		
; Setup Mode in EXTMODE register
; EXTMODE(0) = 1, other bits unchanged
		; R2 = address of EXTMODE register
		; R3 = current value of EXTMODE
		; set bit 0 of EXTMODE to 1
		; write back to EXTMODE register
		
; Setup Polarity in EXTPOLAR register
; EXTPOLAR(0) = 1, other bits unchanged
		; R2 = address of EXTPOLAR register
		; R3 = current value of EXTPOLAR
		; set bit 0 of EXTPOLAR to 1
		; write back to EXTPOLAR
		
; Step 2: Enable interrupt for peripheral (EINT0) in ISER0 register
; ISER(18) = 1, other bits unchanged
		; R2 = address of ISER0 register
		; R3 = current value of ISER0
		; set bit 18 of ISER0 to 1
		; write back to ISER0
		
; Ignore steps 3 and 4, defaults are ok
		BX LR	; return to main from subroutine
		ENDP
		
		EXPORT EINT0_IRQHandler
EINT0_IRQHandler PROC

; Clear EINT0 interrupt flag
		; R0 = address of EXTINT register
		; R1 = 1 for clearing EINT0
		; clear EINT0 flag by writing 1 to bit 0
		
; Increment count
		LDR R0, =count			; R0 = address of count in memory
		; R1 = current count (value)
		; Increment R1
		; Store R1 back into count in memory
		MOV R0, R1				; Call LEDs subroutine
		PUSH {LR}
		BL LEDs
		POP {LR}
		ENDP

; Initializes FIO1DIR and FIO2DIR
Init_LEDs PROC
		LDR R0, =0x2009C020 ; FIO1DIR address
		LDR R1, =0xF0000000 ; Bits 31:28
		STR R1, [R0]
		MOVW R1, 0x007C		; Bits 6:2
		STR R1, [R0,#0x20]	; FIO2DIR = FIO1DIR + 0x20
		BX LR
		ENDP

; Show a binary number on the LEDs
; Assumes that R0 holds the number to show
LEDs	PROC
		MOV R3, #0
		AND R1, R0, #0x80
		LSL R1, R1, #21
		ORR R3, R3, R1
		AND R1, R0, #0x40
		LSL R1, R1, #23
		ORR R3, R3, R1
		AND R1, R0, #0x20
		LSL R1, R1, #26
		ORR R3, R3, R1
		LDR R2, =0x2009C034
		STR R3, [R2]
		
		MOV R3, #0
		AND R1, R0, #0x10
		LSR R1, R1, #2
		ORR R3, R3, R1
		AND R1, R0, #0x08
		ORR R3, R3, R1
		AND R1, R0, #0x04
		LSL R1, R1, #2
		ORR R3, R3, R1
		AND R1, R0, #0x02
		LSL R1, R1, #4
		ORR R3, R3, R1
		AND R1, R0, #0x01
		LSL R1, R1, #6
		ORR R3, R3, R1
		STR R3, [R2, #0x20]
		BX LR
		ENDP

		END