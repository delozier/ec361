#include "LPC17xx.h"
#define STCTRL_ADR 0xE000E010
#define STRELOAD_ADR 0xE000E014
#define STCURR_ADR 0xE000E018

void SysTick_Init();
void SysTick_Handler();

int main() {
	volatile unsigned int* FIO1DIR_ptr = (unsigned int*) 0x2009C020;
	*(FIO1DIR_ptr) = 0x10000000;
	SysTick_Init();
	
	while(1) {}
	return 0;
}

void SysTick_Init(){
	volatile unsigned int *STCTRL_ptr = (unsigned int*)STCTRL_ADR;
	volatile unsigned int *STRELOAD_ptr = (unsigned int*)STRELOAD_ADR;
	volatile unsigned int *STCURR_ptr = (unsigned int*)STCURR_ADR;
	
	// Disable timer
	// Set reload value = 50 ms (100 MHz)
	// Clear STCURR (write anything)
	// Set ENABLE = 1, TICKINT = 1, CLKSOURCE = 1
	
	return;
}

void SysTick_Handler(){
	volatile unsigned int *STCTRL_ptr = (unsigned int*)STCTRL_ADR;
	volatile unsigned int *FIO1PIN_ptr = (unsigned int*)0x2009C034;
	
	// Toggle P1.28 LED
	// Read STCTRL register to clear
	
	return;
}